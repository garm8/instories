## Для запуска локально нужно выполнить следующие шаги:
- Склонировать проект
`git clone https://gitlab.com/garm8/instories.git`
- Установить и активировать виртуальное окружение с завивимостями из файла requirements.txt  
- Из окружения запустить Celery  
`celery --app video.infrastructure.celery_app worker --loglevel=INFO`
- Запустить контейнер с Redis  
`docker run -d -p 6379:6379 redis`  
- Вывполнить миграции, создастся БД SQLite  
`python3 manage.py migrate`
- Запустить Django  
`python3 manage.py runserver 0.0.0.0:8000`
- Открыть стартовую страницу  
`0.0.0.0:8000/videos/`
