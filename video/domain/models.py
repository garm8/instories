import uuid
from django.db import models
from django.utils import timezone


class Video(models.Model):
    guid = models.UUIDField(primary_key=True, default=uuid.uuid4())
    date = models.DateTimeField(default=timezone.now())
    file = models.FileField(null=True)
    text = models.CharField(max_length=4096, null=True)

    class Meta:
        db_table = 'uploaded_videos'
        ordering = ['guid', ]


class Task(models.Model):
    guid = models.UUIDField(primary_key=True, default=uuid.uuid4())
    video = models.OneToOneField(Video, on_delete=models.CASCADE, related_name='task')

    class Meta:
        db_table = 'tasks'
        ordering = ['guid', ]


class PreviewPicture(models.Model):
    guid = models.UUIDField(primary_key=True, default=uuid.uuid4())
    date = models.DateTimeField(default=timezone.now())
    picture = models.CharField(max_length=1024, null=True)
    video = models.OneToOneField(Video, on_delete=models.CASCADE, related_name='preview_picture')

    class Meta:
        db_table = 'preview_pictures'
        ordering = ['guid', ]


class PreviewAnimation(models.Model):
    guid = models.UUIDField(primary_key=True, default=uuid.uuid4())
    date = models.DateTimeField(default=timezone.now())
    animation = models.CharField(max_length=1024, null=True)
    video = models.OneToOneField(Video, on_delete=models.CASCADE, related_name='preview_animation')

    class Meta:
        db_table = 'preview_animations'
        ordering = ['guid', ]


class RenderedVideo(models.Model):
    guid = models.UUIDField(primary_key=True, default=uuid.uuid4())
    date = models.DateTimeField(default=timezone.now())
    rendered_video = models.CharField(max_length=1024, null=True)
    video = models.OneToOneField(Video, on_delete=models.CASCADE, related_name='rendered_video')

    class Meta:
        db_table = 'rendered_videos'
        ordering = ['guid', ]
