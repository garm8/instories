from celery.utils.log import get_task_logger
from video.domain.services.video_manager import VideoManager
from video.infrastructure.celery_app import app


logger = get_task_logger(__name__)


@app.task(bind=True, name='process_video')
def process_video(self, file: str, video_guid: str) -> bool:
    try:
        VideoManager(file, video_guid).process_video()
        return True
    except Exception as exc:
        raise self.retry(exc=exc, countdown=60)
