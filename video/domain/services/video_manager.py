import uuid
from moviepy import editor
import random
from django.utils import timezone
from instories.settings import BASE_DIR, MEDIA_URL
from video.domain.models import PreviewPicture, Video, PreviewAnimation


class VideoManager:

    # SUB_CLIP_DURATION = 5
    # VIDEO_HEIGHT = 180
    # PREVIEW_NAME = 'preview'
    # ANIMATION_FPS = 10

    SUB_CLIP_DURATION = 10
    VIDEO_HEIGHT = 400
    PREVIEW_NAME = 'preview_'
    ANIMATION_FPS = 60

    def __init__(self, file: str, video_guid: str) -> None:
        self.video = Video.objects.get(guid=video_guid)
        self.video_file = editor.VideoFileClip(file)

    def process_video(self) -> None:
        self._prepare_video()
        sub_clip_start, sub_clip_end = self._get_timings()
        self._get_preview_picture(sub_clip_start)
        self._get_preview_animation(sub_clip_start, sub_clip_end)

    def _prepare_video(self) -> None:
        self.video_file = self.video_file.resize(height=self.VIDEO_HEIGHT)

    def _get_timings(self) -> (float, float):
        sub_clip_end = random.uniform(self.SUB_CLIP_DURATION, self.video_file.duration)
        sub_clip_start = sub_clip_end - self.SUB_CLIP_DURATION
        return sub_clip_start, sub_clip_end

    def _get_preview_picture(self, sub_clip_start: float) -> None:
        filename = self.PREVIEW_NAME + str(self.video.guid) + '.jpg'
        self.video_file.save_frame(filename=str(BASE_DIR) + str(MEDIA_URL) + filename, t=sub_clip_start)
        preview_picture_data = {
            'guid': uuid.uuid4(),
            'date': timezone.now(),
            'picture': filename,
            'video': self.video,
        }
        PreviewPicture.objects.create(**preview_picture_data)

    def _get_preview_animation(self, sub_clip_start: float, sub_clip_end: float) -> None:
        animation = self.video_file.subclip(t_start=sub_clip_start, t_end=sub_clip_end)
        filename = self.PREVIEW_NAME + str(self.video.guid) + '.gif'
        animation.write_gif(filename=str(BASE_DIR) + str(MEDIA_URL) + filename, fps=self.ANIMATION_FPS)
        preview_animation_data = {
            'guid': uuid.uuid4(),
            'date': timezone.now(),
            'animation': filename,
            'video': self.video,
        }
        PreviewAnimation.objects.create(**preview_animation_data)
