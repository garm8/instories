from django.apps import AppConfig


class VideoConfig(AppConfig):
    name = 'video'

    def ready(self):
        from .domain.models import Video, RenderedVideo, PreviewAnimation, PreviewPicture, Task
