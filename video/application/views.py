import datetime
import uuid
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from instories.settings import BASE_DIR, MEDIA_URL
from video.domain.models import Video, Task, PreviewAnimation, PreviewPicture
from video.domain.services.tasks import process_video
from video.infrastructure.celery_app import app


def start_page(request):
    videos = Video.objects.all().order_by('-date')
    return render(
        request,
        'start_page.html',
        context={'videos': videos},
    )


def upload_page(request):

    if request.method == 'POST':
        initial_data = {
            'date': datetime.datetime.now(),
            'guid': uuid.uuid4(),
            'text': request.POST.get('text'),
            'file': request.FILES.get('file'),
        }
        video = Video.objects.create(**initial_data)
        file = str(BASE_DIR) + '/media/' + str(video.file)
        video_guid = str(video.guid)
        upload_task = process_video.delay(file, video_guid)
        task = Task.objects.create(**{'guid': upload_task.id, 'video': video})
        return HttpResponseRedirect(reverse('start_page'))
    else:
        return render(request, 'upload_page.html')


def status_video(request, task_id):

    if request.method == 'GET':
        task_result = app.AsyncResult(id=task_id)
        result = {
            'celery_task_id': task_id,
            'state': task_result.state,
        }
        return JsonResponse(data=result, status=200, safe=False)


def get_animation(request, video_guid):

    if request.method == 'GET':
        video = Video.objects.get(guid=video_guid)
        if animations := PreviewAnimation.objects.filter(video=video).order_by('-date'):
            animation = animations[0]
            with open(str(BASE_DIR) + str(MEDIA_URL) + animation.animation, 'rb') as animation_file:
                response = HttpResponse(
                    animation_file.read(),
                    content_type='image/gif'
                )
                return response
        else:
            result = {
                'result': 'not ready',
            }
            return JsonResponse(data=result, safe=False)


def get_picture(request, video_guid):

    if request.method == 'GET':
        video = Video.objects.get(guid=video_guid)
        if pictures := PreviewPicture.objects.filter(video=video).order_by('-date'):
            picture = pictures[0]
            with open(str(BASE_DIR) + str(MEDIA_URL) + picture.picture, 'rb') as picture_file:
                response = HttpResponse(
                    picture_file.read(),
                    content_type='image/jpg'
                )
                return response
        else:
            result = {
                'result': 'not ready',
            }
            return JsonResponse(data=result, safe=False)


def get_video(request, video_guid):

    if request.method == 'GET':
        video = Video.objects.get(guid=video_guid)
        with open(str(BASE_DIR) + str(MEDIA_URL) + str(video.file), 'rb') as video_file:
            response = HttpResponse(
                video_file.read(),
                content_type='video/mp4'
            )
            return response
