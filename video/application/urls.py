from django.urls import path
from video.application.views import start_page, upload_page, status_video, get_animation, get_picture, get_video


urlpatterns = [
    path('', start_page, name='start_page'),
    path('upload_file/', upload_page, name='upload_page'),
    path('status_video/<task_id>/', status_video, name='status_video'),
    path('<video_guid>/get_picture/', get_picture, name='get_picture'),
    path('<video_guid>/get_animation/', get_animation, name='get_animation'),
    path('<video_guid>/get_video/', get_video, name='get_video'),
]
