from django import forms
from django.utils import timezone

from video.domain.models import Video
from django import forms
import uuid

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import datetime #for checking renewal date range.


class VideoForm(forms.ModelForm):
    # file = forms.FileField()

    class Meta:
        model = Video
        fields = ('file', )


class VideoNewForm(forms.Form):
    # guid = forms.UUIDField(label='uuid')
    # date = forms.DateTimeField()
    file = forms.FileField(label='file')
