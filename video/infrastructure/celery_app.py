import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'instories.settings')
app = Celery('video', backend='redis://0.0.0.0:6379')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
