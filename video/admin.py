from django.contrib import admin
from video.domain.models import Video, RenderedVideo, PreviewAnimation, PreviewPicture, Task


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    list_display = ('guid', 'date', 'file', 'text')


@admin.register(RenderedVideo)
class RenderedVideoAdmin(admin.ModelAdmin):
    list_display = ('guid', 'date', 'rendered_video', 'video')


@admin.register(PreviewAnimation)
class VideoAdmin(admin.ModelAdmin):
    list_display = ('guid', 'date', 'animation', 'video')


@admin.register(PreviewPicture)
class VideoAdmin(admin.ModelAdmin):
    list_display = ('guid', 'date', 'picture', 'video')


@admin.register(Task)
class VideoAdmin(admin.ModelAdmin):
    list_display = ('guid', 'video')
