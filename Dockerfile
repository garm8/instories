FROM python:3.9

RUN pip install --upgrade pip

RUN mkdir -p /usr/src/instories/

WORKDIR /usr/src/instories/

COPY . .

RUN pip3 install -U -r requirements.txt

EXPOSE 8001

CMD ["python3", "manage.py", "runserver", "0.0.0.0:8001"]
